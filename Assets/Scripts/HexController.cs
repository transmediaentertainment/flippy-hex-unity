﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HexController : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        forwardMoveVec = transform.position + forwardMoveVec;
        var leftRot = Quaternion.AngleAxis(60f, Vector3.down);
        leftMoveVec = leftRot * forwardMoveVec;
        leftMoveAxis = leftRot * forwardMoveAxis;
        var rightRot = Quaternion.AngleAxis(60f, Vector3.up);
        rightMoveVec = rightRot * forwardMoveVec;
        rightMoveAxis = rightRot * forwardMoveAxis;

        CheckNextFlip();
	}

    public float t = 0f;
    float lastT = 0f;
    public float deltaT;

    public float m_FlipSpeed = 1f;

    float totalAngle = 180f;
    float currentAngle = 0f;
    float angularSpeed = 180f;
    Vector3 rotateAroundPos = new Vector3(0, 0f, 0.25f);
    Vector3 rotateAroundAxis = new Vector3(1f, 0f, 0f);

    Vector3 forwardMoveVec = new Vector3(0f, 0f, 0.25f);
    Vector3 forwardMoveAxis = new Vector3(1f, 0f, 0f);
    Vector3 leftMoveVec;
    Vector3 leftMoveAxis;
    Vector3 rightMoveVec;
    Vector3 rightMoveAxis;

    public Ease m_EaseType;

    public ParticleSystem m_Particles;

    public int m_XGridPos = 0;
    public int m_YGridPos = 0;


    private void CheckNextFlip()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            FlipLeft();
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            FlipRight();
        }
        else
        {
            FlipForward();
        }
        t = 0f;
        lastT = 0f;
        DOTween.To(() => t, (x) => { t = x; UpdateFlip(); }, 1f, m_FlipSpeed).SetEase(m_EaseType).OnComplete(() => { DustParticles(); CheckNextFlip(); });
    }

    private void DustParticles()
    {
        m_Particles.transform.position = transform.position;
        m_Particles.Emit(50);
    }

    private void UpdateFlip()
    {
        deltaT = t - lastT;
        float deltaAngle = angularSpeed * deltaT;
        if (currentAngle + deltaAngle > totalAngle)
        {
            deltaAngle = totalAngle - currentAngle;
        }
        currentAngle += deltaAngle;
        transform.RotateAround(rotateAroundPos, rotateAroundAxis, deltaAngle);
        lastT = t;
    }

    void FlipForward()
    {
        currentAngle = 0f;
        m_YGridPos++;
        rotateAroundPos = transform.position + forwardMoveVec;
        rotateAroundAxis = forwardMoveAxis;
    }

    void FlipLeft()
    {
        currentAngle = 0f;
        m_XGridPos--;
        if (m_XGridPos % 2 != 0)
        {
            m_YGridPos++;
        }
        rotateAroundPos = transform.position + leftMoveVec;
        rotateAroundAxis = leftMoveAxis;
    }
    void FlipRight()
    {
        currentAngle = 0f;
        m_XGridPos++;
        if (m_XGridPos % 2 != 0)
        {
            m_YGridPos++;
        }
        rotateAroundPos = transform.position + rightMoveVec;
        rotateAroundAxis = rightMoveAxis;
    }

}
