﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

    public Vector3 m_Offset;
    public Transform m_Target;
    public float m_ForwardOffset = 2f;

	void LateUpdate()
    {
        var pos = m_Target.position;
        pos.y = 0;
        transform.position = pos + m_Offset;

        transform.LookAt(pos + new Vector3(0f, 0f, m_ForwardOffset));
    }
}
