﻿using UnityEngine;
using System.Collections;

public class WorldGenerator : MonoBehaviour
{
    public GameObject m_HexObject;
    private const float m_HexMeshSize = 0.5f;

    public int xSize = 5;
    public int ySize = 5;

    private float m_XOffset;
    private float m_YOffset;
    private float m_YMod;

    private GameObject[,] m_HexGrid;

    public Material m_EvenY;
    public Material m_OddY;
 
    void Awake()
    {
        Vector3 offset = new Vector3(0f, 0f, 0.5f);
        Vector3 rotOffset = Quaternion.AngleAxis(60f, Vector3.up) * offset;
        m_XOffset = rotOffset.x;
        m_YOffset = m_HexMeshSize;
        m_YMod = rotOffset.z;

        Debug.Log("Offset : " + rotOffset);
        Debug.Log("Y Mod : " + m_YMod);
        m_HexGrid = new GameObject[xSize, ySize];

        for(int x = 0; x < xSize; x++)
        {
            bool useMod = x % 2 == 1;
            for(int y = 0; y < ySize; y++)
            {
                var go = (GameObject)Instantiate(m_HexObject);
                if (y % 2 == 0)
                {
                    go.renderer.sharedMaterial = m_EvenY;
                }
                else
                {
                    go.renderer.sharedMaterial = m_OddY;
                }
                go.transform.rotation = Quaternion.Euler(0f, 30f, 0f);
                if (Random.Range(0, 2) % 2 == 0)
                {
                    go.transform.localScale = new Vector3(1f, 15f, 1f);
                }
                go.transform.position = new Vector3(m_XOffset * x, 0f, m_YOffset * y + (useMod ? m_YMod : 0f));
            }
        }
    }

    
}
